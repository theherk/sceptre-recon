package recon_test

import (
	"github.com/spf13/afero"
	"gitlab.com/theherk/sceptre-recon/pkg/recon"
)

func ExampleAll() {
	fs := afero.NewMemMapFs()
	fs.MkdirAll("sceptre/templates", 0755)
	fs.MkdirAll("sceptre/config", 0755)
	recon.Fs = fs
	recon.All("sceptre")
	// Output: sceptre: sceptre root found; checking
}

func ExampleAll_unusedTemplate() {
	fs := afero.NewMemMapFs()
	fs.MkdirAll("sceptre/templates", 0755)
	fs.MkdirAll("sceptre/config", 0755)
	afero.WriteFile(fs, "sceptre/templates/tmplA.yaml", []byte(""), 0644)
	recon.Fs = fs
	recon.All("sceptre")
	// Output: sceptre: sceptre root found; checking
	// sceptre/templates/tmplA.yaml: template exists, but is not used
}

func ExampleAll_missingTemplate() {
	fs := afero.NewMemMapFs()
	fs.MkdirAll("sceptre/templates", 0755)
	fs.MkdirAll("sceptre/config", 0755)
	afero.WriteFile(fs, "sceptre/config/cfgA.yaml", []byte("template_path: tmplA.yaml"), 0644)
	recon.Fs = fs
	recon.All("sceptre")
	// Output: sceptre: sceptre root found; checking
	// sceptre/config/cfgA.yaml: template missing; tmplA.yaml
}
