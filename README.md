# sceptre-recon

Reconcile a [sceptre](https://github.com/Sceptre/sceptre) directory's templates and configurations. This is not robust, and probably won't be, but it serves as a simple tool to ensure each configuration uses a template that exists, and that every template is called by some configuration.

Currently, it simply tells you of discrepencies. It is up to you to resolve them.

## Installation

    go get gitlab.com/theherk/sceptre-recon

## Usage

`sceptre-recon` can be called without arguments, in which case it will assume the current directory is a sceptre root, or with _n_ arguments, in which case each argument is expected to be the path to a sceptre root.

#### check the current directory

    sceptre-recon

#### check multiple directories

    sceptre-recon ~/projects/a ~/projects/b
