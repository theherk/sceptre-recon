module gitlab.com/theherk/sceptre-recon

go 1.15

require (
	github.com/spf13/afero v1.4.1
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776
)
