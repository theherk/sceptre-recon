// Package recon reconciles sceptre directories.
package recon

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"

	"github.com/spf13/afero"
	"gopkg.in/yaml.v3"
)

// Fs is the filesystem.
// Setting this allows an in-memory filesystem for testing.
var Fs afero.Fs

type sceptre struct {
	afs   *afero.Afero
	dir   string
	tmpls []string
	cfgs  map[string]string
}

// All checks.
func All(d string) {
	s := &sceptre{dir: d}
	if err := s.init(); err != nil {
		fmt.Println(err)
		return
	}
	s.checkTmpls()
	s.checkCfgs()
	s.checkStacks()
}

func (s *sceptre) cfgTmpl(p string) string {
	f, err := s.afs.ReadFile(p)
	if err != nil {
		fmt.Printf("%s: error reading config", p)
	}
	tmplLine := regexp.MustCompile(`(?m)^template_path:.*$`)
	partial := tmplLine.Find(f)
	cfg := struct {
		TemplatePath string                 `yaml:"template_path"`
		Parameters   map[string]interface{} `yaml:"-"`
	}{}
	if err := yaml.Unmarshal(partial, &cfg); err != nil {
		fmt.Printf("Unmarshal: %v\n", err)
	}
	return cfg.TemplatePath
}

func (s *sceptre) checkCfgs() {
	for c, t := range s.cfgs {
		// Verify the template used exists.
		tPath := fmt.Sprintf("%s/templates/%s", s.dir, t)
		b, err := s.afs.Exists(tPath)
		if err != nil {
			fmt.Println(err)
		}
		if !b {
			fmt.Printf("%s: template missing; %s\n", c, t)
		}
	}
}

func (s *sceptre) checkStacks() {
	// not implemented
}

func (s *sceptre) checkTmpls() {
	allUsed := make(map[string]bool)
	for _, t := range s.cfgs {
		allUsed[fmt.Sprintf("%s/templates/%s", s.dir, t)] = true
	}
	for _, t := range s.tmpls {
		// Verify the is at least one configuration using this config.
		if _, ok := allUsed[t]; !ok {
			fmt.Printf("%s: template exists, but is not used\n", t)
		}
	}
}

func (s *sceptre) getCfgs() {
	s.cfgs = make(map[string]string)
	if err := s.afs.Walk(s.dir+"/config", func(p string, info os.FileInfo, err error) error {
		if filepath.Ext(p) == ".yml" || filepath.Ext(p) == ".yaml" {
			s.cfgs[p] = s.cfgTmpl(p)
		}
		return nil
	}); err != nil {
		fmt.Printf("%s: error finding templates\n", s.dir)
	}
}

func (s *sceptre) getTmpls() {
	if err := s.afs.Walk(s.dir+"/templates", func(p string, info os.FileInfo, err error) error {
		if filepath.Ext(p) == ".yml" || filepath.Ext(p) == ".yaml" {
			s.tmpls = append(s.tmpls, p)
		}
		return nil
	}); err != nil {
		fmt.Printf("%s: error finding templates\n", s.dir)
	}
}

func (s *sceptre) init() error {
	if Fs == nil {
		Fs = afero.NewOsFs()
	}
	s.afs = &afero.Afero{Fs: Fs}
	if !s.sceptreRoot(s.dir) {
		return fmt.Errorf("%s: not a sceptre root\n", s.dir)
	}
	fmt.Printf("%s: sceptre root found; checking\n", s.dir)
	s.getTmpls()
	s.getCfgs()
	return nil
}

func (s *sceptre) sceptreRoot(d string) bool {
	tmpl := false
	cfg := false
	out, err := s.afs.ReadDir(d)
	if err != nil {
		return false
	}
	for _, f := range out {
		if f.IsDir() && f.Name() == "templates" {
			tmpl = true
		}
		if f.IsDir() && f.Name() == "config" {
			cfg = true
		}
	}
	return tmpl && cfg
}
