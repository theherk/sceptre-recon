/*
- directory if given, curdir if not
- check for ./templates and ./config
- get list of all templates
- get map[string]string of config{template}
- verify each config template exists
- verify each template has at least one assiciated config
- verify each config has associated stack
*/
package main

import (
	"os"

	"gitlab.com/theherk/sceptre-recon/pkg/recon"
)

func main() {
	dirs := os.Args[1:]
	if len(dirs) == 0 {
		d, err := os.Getwd()
		if err != nil {
			panic(err)
		}
		dirs = append(dirs, d)
	}
	for _, d := range dirs {
		recon.All(d)
	}
}
